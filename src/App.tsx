import React, { useState } from 'react';
import { MyPage } from './MyPage';
import { Navbar } from './Navbar';

export type Page = 'catalog' | 'effects' | 'uikit' | 'events' | 'state' | 'crud' | 'uncontrolled-forms' | 'controlled-forms'

const App = () => {
  const [page, setPage] = useState<Page | null>('crud')

  function changePage(p: Page) {
    setPage(p)
  }

  return (
    <div className="m-3">
      <Navbar onChangePage={changePage }/>
      { page && <MyPage value={page}/>}
    </div>
  )
}
export default App;

