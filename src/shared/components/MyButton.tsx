import React, { PropsWithChildren } from 'react';

interface MyButtonProps {
  disabled?: boolean;
  onDoClick?: (e: React.MouseEvent) => void;
}

const css: React.CSSProperties = {
  border: '1px solid blue',
  padding: '10px',
  display: 'inline-block',
}

export const MyButton = React.memo((props: PropsWithChildren<MyButtonProps>) => {
  console.log('render Button')
  return (
    <span
      style={{
        ...css,
        opacity: props.disabled ? 0.2 : 1,
        cursor: props.disabled ? 'auto' : 'pointer'
      }}
      onClick={props.onDoClick}
    >
      {props.children}
    </span>
  )
})
