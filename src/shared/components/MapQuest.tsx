interface MapQuestProps {
  city: string;
  zoom?: number;
}

const APIKEY = 'Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn';
const BASEPATH = 'https://www.mapquestapi.com/staticmap/v5/map';

//export default function MapQuest(props: MapQuestProps) {
export default function MapQuest(props: MapQuestProps) {
  const { city, zoom: z = 10 } = props;
  return <div>
    <img
      width="100%"
      src={`${BASEPATH}?key=${APIKEY}&center=${city}&size=600,400&zoom=${z}`} alt=""/>
  </div>
}
