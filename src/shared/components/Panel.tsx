import React, { PropsWithChildren } from 'react';
import cn from 'classnames';

interface PanelProps {
  title: string;
  marginBottom?: boolean;
  theme?: 'dark' | 'light';
  icon?: string;
  onIconClick?: () => void;
  // children: React.ReactNode;
}

export const Panel = (props: PropsWithChildren<PanelProps>) => {
  return (
    <div className={cn('card', { 'mb-3': props.marginBottom })}>
      <div className={
        cn('card-header', {
          'bg-dark text-white': props.theme === 'dark',
          'bg-secondary': props.theme === 'light',
        })}
      >
        {props.title}
        {
          props.icon && (
            <div className="pull-right">
              <i className={props.icon} onClick={props.onIconClick}></i>
            </div>
          )
        }
      </div>
      <div className="card-body">{props.children}</div>
    </div>
  )
}
