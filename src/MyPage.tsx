
// MyPage.tsx
import { StateDemo1 } from './pages/StateDemo1';
import { EffectsDemo } from './pages/EffectsDemo';
import { Catalog } from './pages/Catalog';
import UiKitDemoPage from './pages/UiKitDemoPage';
import EventsDemoPage from './pages/EventsDemoPage';
import { Crud } from './pages/crud/Crud';
import React, { useEffect } from 'react';
import { Page } from './App';
import { UncontrolledFormsDemo } from './pages/UncontrolledFormsDemo';
import { ControlledFormsDemo } from './pages/ControlledFormsDemo';

interface MyPageProps {
  value: Page;
}

export function MyPage(props: MyPageProps) {
  function renderPage() {
    switch (props.value) {
      case 'state': return <StateDemo1/>;
      case 'effects': return <EffectsDemo/>;
      case 'catalog': return <Catalog/>;
      case 'uikit': return <UiKitDemoPage/>;
      case 'events': return <EventsDemoPage/>;
      case 'crud': return <Crud />;
      case 'uncontrolled-forms': return <UncontrolledFormsDemo />;
      case 'controlled-forms': return <ControlledFormsDemo />;
      // default: return <div>Seleziona una pagina del menu</div>
    }
  }

  return <div>
    {renderPage()}
  </div>
}
