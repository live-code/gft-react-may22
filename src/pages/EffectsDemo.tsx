import { useEffect, useState } from 'react';

export function EffectsDemo() {
  const [count, setCount] = useState(0);
  const [users, setUsers] = useState<number[]>([]);

  // change count
  useEffect(() => {
    console.log('http...');
    // destroy / cleanup
    return () => {
      console.log('destroy')
    }
  }, []) // onChange


  return <div >
    Effects Demo {count} - {users.length}
    <button onClick={() => setUsers([1, 2, 3])}>set array</button>
    <button onClick={ () => setCount(c => c + 1) }>+</button>
  </div>
}
