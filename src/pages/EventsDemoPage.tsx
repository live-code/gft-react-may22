import React from 'react';

const EventsDemoPage = () => {
  function goto(url: string, event: React.MouseEvent) {
    console.log(event.clientX)
    window.open(url)
  }

  function goto2(event: React.MouseEvent<HTMLButtonElement>) {
    window.open(event.currentTarget.dataset.url)
  }

  function keydownHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      window.open('https://it.wikipedia.org/wiki/' + e.currentTarget.value)
    }
  }

  return (
    <div>
      <input onKeyDown={keydownHandler} type="text" placeholder="Write something"/>
      <br/>
      <button onClick={e => goto('http://www.google.com', e)}>
        Go to Google
      </button>
      <button
        onClick={goto2}
        data-url="http://www.facebook.com"
      >
        Go to FB
      </button>

    </div>
  )
}
export default EventsDemoPage;

