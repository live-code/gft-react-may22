import { useState } from 'react';
import cn from 'classnames';
import { validate } from '../utils/form-validators';

const INITIAL_STATE = { name: 'abc', city: '', gender: ''};

export function ControlledFormsDemo() {
  const [formData, setFormData] = useState(INITIAL_STATE)

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  function  save(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    console.log(formData)
  }

  const {
    isFormValid, isCityValid, isGenderValid, isNameValid
  } = validate(formData)

  return <div>
    <h1>Controlled forms </h1>

    <form onSubmit={save}>
      { isFormValid ? 'form valido' : 'form non valido' }
      <input
        type="text"
        className={cn('form-control', { 'is-valid': isNameValid, 'is-invalid': !isNameValid}  )}
        value={formData.name}
        onChange={onChangeHandler}
        placeholder="name"
        name="name"
      />

      <input
        type="text"
        className={cn('form-control', { 'is-valid': isCityValid, 'is-invalid': !isCityValid}  )}
        value={formData.city}
        onChange={onChangeHandler}
        placeholder="city"
        name="city"
      />

      <select
        className={cn('form-control', { 'is-valid': isGenderValid, 'is-invalid': !isGenderValid}  )}
        name="gender"
        value={formData.gender} onChange={onChangeHandler}>
        <option value="">Select Gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>
      <button type="submit" disabled={!isFormValid}>SAVE</button>

      <pre>{JSON.stringify(formData, null, 2)}</pre>
    </form>
  </div>
}
