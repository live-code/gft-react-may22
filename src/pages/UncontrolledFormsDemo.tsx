import { useEffect, useRef } from 'react';

export function UncontrolledFormsDemo() {
  const inputEl = useRef<HTMLInputElement | null>(null);

  useEffect(() => {
    inputEl.current?.focus()
  }, [])

  function save() {
    if (inputEl.current && inputEl.current.value.length > 3) {
      const params = {
        name: inputEl.current?.value,
        surname: ''
      }
      console.log('http', params)
    }
  }

  return <div>
    <h1>Uncontrolled Forms</h1>
    <input
      type="text" className="form-control"
      ref={inputEl}
    />
    <button onClick={save}>SAVE</button>
  </div>
}
