import { useState } from 'react';
import classNames from 'classnames';

interface Coords {
  x: number;
  y: number;
}

export function StateDemo1() {
  const [counter, setCounter] = useState<number>(0)
  const [username, setUsername] = useState<string>('Fabio')
  const [coords, setCoords] = useState<Coords>({x: 0, y: 0})

  const inc = () => {
    setCounter(c => c + 1)
  }

  const keyDownHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      setUsername(e.currentTarget.value)
    }
  }

  function mouseMoveHandler(e: React.MouseEvent) {
    setCoords({ x: e.clientX / 2, y: e.clientY })
  }
  const isValid: boolean = username.length > 3

  return <div onMouseMove={mouseMoveHandler}>
    {counter}

    <div className={classNames({'is-invalid': !isValid})}>{username}</div>
    <br/>
    <div
      className="mypopover"
      style={{ left: coords.x, top: coords.y}}
    >Tooltip</div>
    <br/>
    <input type="text" onKeyDown={keyDownHandler}/>
    <button onClick={inc}>+</button>
  </div>
}
