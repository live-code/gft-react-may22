import { useEffect, useState } from 'react';
import { User } from '../../../model/user';
import axios from 'axios';

const API = 'http://localhost:3001';

export function useUsers() {
  const [users, setUsers] = useState<User[]>([]);
  const [activeUser, setActiveUser] = useState<Partial<User>>({ name: '' });
  const [error, setError] = useState<boolean>(false)

  useEffect(() => {
    getAll();
  }, [])

  function getAll() {
    axios.get<User[]>(`${API}/users`)
      .then(res => {
        setUsers(res.data)
      })
      .catch(err => {
        setError(true)
      })
  }
  function deleteUser(id: number) {
    axios.delete(`${API}/users/${id}`)
      .then(() => {
        setUsers(s => s.filter(u => u.id !== id));
        setActiveUser({ name: ''})
      })
  }

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    // setActiveUser({ ...activeUser, name: e.target.value})
    setActiveUser(s =>  ({...s, name: e.target.value}))
  }

  function addUser() {
    setError(false)
    axios.post<User>(`${API}/users/`, activeUser )
      .then((res) => {
        setUsers([...users, res.data])
        setActiveUser({ name: '' })
      })
      .catch(err => {
        setError(true)
      })
  }

  function editUser() {
    axios.patch<User>(`${API}/users/${activeUser?.id}`, activeUser )
      .then((res) => {
        setUsers(
          users.map(u => {
            if (u.id === activeUser?.id) {
              return res.data;
            }
            return u;
          })
        )
      })
  }

  function saveUser() {
    if (activeUser?.id) {
      editUser();
    } else {
      addUser()
    }
  }

  function selectUser(u: User) {
    setActiveUser(u)
  }

  return {
    users,
    activeUser,
    error,
    actions: {
      saveUser,
      deleteUser,
      selectUser,
      onChangeHandler,
    }
  };
}
