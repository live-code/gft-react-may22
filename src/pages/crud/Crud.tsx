import { UsersList } from './components/UsersList';
import { UsersForm } from './components/UsersForm';
import { useUsers } from './hooks/useUsers';
import { useResize } from '../../shared/hooks/useResize';
import { useMediaQuery } from '@react-hook/media-query';
import { Accordion, AccordionDetails, AccordionSummary, Typography } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Button from '@mui/material/Button';

export function Crud() {
  const { users, activeUser, error, actions } = useUsers();
  const {  width = 0, height  } = useResize();

  const matches = useMediaQuery('only screen and (min-width: 800px)')

  function doSomething() {
  }

  return <div>
    {error && <div className="alert alert-danger">AHIA!</div>}
    <br/>
    { matches ? <div>big</div> : <div>small</div>}

    <div style={{ background: 'red', width: width/2}}>.....</div>

    <UsersForm
      activeUser={activeUser}
      onSave={actions.saveUser}
      onChangeUser={actions.onChangeHandler}
    />


    <button onClick={doSomething}></button>
    <button onClick={function () {
      doSomething()
    }}></button>

    <UsersList
      data={users}
      activeUser={activeUser}
      onDeleteUser={actions.deleteUser}
      onItemClick={actions.selectUser}
    />

    <br/>
    <br/>
    <br/>

    {
      users.map(user => {
        return (
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon/>}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>{user.name}</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                {user.phone} <br/>
                {user.username}

                <br/>
                <Button variant="outlined" onClick={() => actions.deleteUser(user.id)}>DELETE</Button>
              </Typography>
            </AccordionDetails>
          </Accordion>
        )
      })

    }

  </div>
}
