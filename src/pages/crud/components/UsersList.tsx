import classNames from 'classnames';
import { User } from '../../../model/user';
import { IconButton, Tooltip } from '@mui/material';

interface UsersListProps {
  data: User[];
  activeUser: Partial<User>;
  onDeleteUser: (id: number) => void;
  onItemClick: (u: User) => void;
}

export function UsersList(props: UsersListProps) {
  return (
    <ul className="list-group">
      {
        props.data.map(u => {
          return <li
            key={u.id}
            className={classNames('list-group-item', { active: u.id === props.activeUser?.id})}
            onClick={() => {
              console.log('setactive')
              props.onItemClick(u)
            }}
          >
            {u.name} - {u.id}
            <div className="pull-right">
              <Tooltip title="Delete">
                <IconButton>
                  <i className="fa fa-trash"
                     onClick={(e: React.MouseEvent) => {
                       e.stopPropagation();
                       props.onDeleteUser(u.id)
                     }}></i>
                </IconButton>
              </Tooltip>
            </div>
          </li>
        })
      }
    </ul>
  )
}
