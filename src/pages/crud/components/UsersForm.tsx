import { User } from '../../../model/user';
import { useState } from 'react';

interface UsersFormProps {
  activeUser: Partial<User>;
  onChangeUser: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onSave: () => void;
}
export function UsersForm(props: UsersFormProps) {
  return (
    <>
      <input type="text" placeholder="add user name"
             onChange={props.onChangeUser}
             value={props.activeUser?.name} />
      <button onClick={props.onSave}>
        { props.activeUser?.id ? 'EDIT' : 'ADD'}
      </button>
    </>
  )
}
