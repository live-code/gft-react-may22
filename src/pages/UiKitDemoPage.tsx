import React from 'react';
import { Panel } from '../shared/components/Panel';
import { MyButton } from '../shared/components/MyButton';
import MapQuest from '../shared/components/MapQuest';
import Slider from "react-slick";
import Button from '@mui/material/Button';

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1
};

const UiKitDemoPage = () => {
  function goto(url: string) {
    window.open(url)
  }
  function doSomething(e: React.MouseEvent) {
    console.log('getProfile', e.clientX)
  }

  function getProfile() {
    console.log('getProfile')
  }
  return (
    <div className="m-3">

      <Button variant="contained">Hello World</Button>

      <MapQuest city="Rome" zoom={15} />
      <MapQuest city="Rome" />

      <MyButton onDoClick={doSomething}>
        <i className="fa fa-times"></i> CLICK
      </MyButton>
      <MyButton disabled={true}>due</MyButton>
      <Panel title="Profile" marginBottom={true}
             onIconClick={getProfile}
      >bla bla</Panel>
      <Panel
        title="Chart"
        theme="light"
        icon="fa fa-google"
        onIconClick={() => goto('http://www.google.com')}
      >...</Panel>

      <Panel
        title="Notification"
        theme="dark"
        icon="fa fa-facebook"
        onIconClick={() => goto('http://www.facebook.com')}
      >
        <input type="text"/>
        <button>CLICK ME</button>
      </Panel>

      <Slider {...settings}>
        <div>
          <h3 className="bg-info">1</h3>
        </div>
        <div>
          <h3 className="bg-warning">2</h3>
        </div>
        <div>
          <h3 className="bg-danger">3</h3>
        </div>
        <div>
          <h3 className="bg-info">4</h3>
        </div>
        <div>
          <h3 className="bg-info">5</h3>
        </div>
        <div>
          <h3 className="bg-info">6</h3>
        </div>
      </Slider>


      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
    </div>
  )
}
export default UiKitDemoPage;
