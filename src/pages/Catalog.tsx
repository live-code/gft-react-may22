import React from 'react';

interface Product {
  id: number;
  name: string;
}

const products: Product[] = [
  { id: 1, name: 'Nutella'},
  { id: 2, name: 'Latte'},
  { id: 3, name: 'Pane'},
];

export const Catalog = () => {
  return (
    <div>
      { products.length > 0 ? <List /> : <Empty /> }
    </div>
  )
}


// List.tsx
const List = () => {
  return <div>
    {
      products.map(p => {
        return <div key={p.id}>{p.name}</div>
      })
    }
  </div>
}

// Empty.tsx
const Empty = () => {
  return <h1>No products</h1>;
}

// components/Panel.tsx
export function Panel() {
  return <div>Sono un panel!!!!</div>
}
