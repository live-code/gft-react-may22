export function validate(formData: any) {
  const isNameValid = formData.name.length > 3;
  const isCityValid = formData.city.length ;
  const isGenderValid = formData.gender !== '';
  const isValid = isNameValid && isCityValid && isGenderValid;

  return {
    isNameValid,
    isCityValid,
    isGenderValid,
    isFormValid: isValid
  }
}
