import React from 'react';
import { Page } from './App';

interface NavbarProps {
  onChangePage: (page: Page) => void;
}
export function Navbar(props: NavbarProps) {
  return <div>
    <button onClick={() => props.onChangePage('catalog')}>Catalog</button>
    <button onClick={() => props.onChangePage('effects')}>Effects</button>
    <button onClick={() => props.onChangePage('uikit')}>uikit</button>
    <button onClick={() => props.onChangePage('events')}>events</button>
    <button onClick={() => props.onChangePage('state')}>state</button>
    <button onClick={() => props.onChangePage('crud')}>crud</button>
    <button onClick={() => props.onChangePage('uncontrolled-forms')}>uncontrolled</button>
    <button onClick={() => props.onChangePage('controlled-forms')}>controlled</button>
    <hr/>
  </div>
}
